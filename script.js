let stringOn;
let score;
let atks;

function reset() {
	stringOn = 3;
	score = 0;
	atks = [];
}

function setup() {
	createCanvas(500, 800);
	stroke(255);
	reset();
}

function board() {
	clear();
	background(255,161,37);
	line(0,700, 600,700);
	for (let i = 1; i <= 4; i++) {line(i*100,0, i*100,800)}
}

function draw() {
	board();
	ellipse((stringOn*100)-50,700, 90,90);
	for (let i = 0; i < atks.length; i++) {
		if (atks[i].string == stringOn && dist(0, 700, 0, atks[i].y) <= 120)  {
			document.getElementById("text").innerHTML = "<div>You loose!</div>Score: "+score;
			atks[i].y = 650;
			atks[i].render();
			alert();
			reset();
			break;
		} else {
			atks[i].fall();
			atks[i].render();
			score += (atks[i].y >= 800);
			atks.splice(i,(atks[i].y >= 800));
		}
	}
}

setInterval(function(){atks.push(new Attacker(9, stringOn))}, 400)

function keyPressed() {stringOn = ((key === "a" && stringOn != 1) ? stringOn -= 1 : (key === "d" && stringOn != 5) ? stringOn += 1 : stringOn)}
